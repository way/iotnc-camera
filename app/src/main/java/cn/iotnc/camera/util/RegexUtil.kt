package cn.iotnc.camera.util

import java.util.regex.Pattern

const val PHONE_NUMBER_REGEX =
    "^((13[0-9])|(15[^4])|(18[0-9])|(17[0-8])|(14[5-9])|(166)|(19[8,9])|)\\d{8}\$"
const val ACCOUNT_REGEX = "^(?![0-9]+\$)[0-9A-Za-z]{5,10}\$"
const val PASSWORD_REGEX =
    "^(?![A-Za-z0-9]+\$)(?![a-z0-9\\W]+\$)(?![A-Za-z\\W]+\$)(?![A-Z0-9\\W]+\$)[a-zA-Z0-9\\W]{8,10}\$"

const val IP_REGEX = ("^(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|[1-9])\\."
        + "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\."
        + "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\."
        + "(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)$")
//const val IP_REGEX = ("^(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|[1-9])"
//        + "(\\.(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)){3}$")

/***
 * 手机号码检测
 */
fun String.checkPhoneNum(): Boolean {
    val p = Pattern.compile(PHONE_NUMBER_REGEX)
    val m = p.matcher(this)
    return m.matches()
}

/**
 * 用户名只能含数字、字母,且不能全为数字,长度5-10位
 */
fun String.checkAccount(): Boolean {
    val p = Pattern.compile(ACCOUNT_REGEX)
    val m = p.matcher(this)
    return m.matches()
}

/**
 * 密码必须包含数字、大小写字母、特殊字符，长度8-10位
 */
fun String.checkPassword(): Boolean {
    val p = Pattern.compile(PASSWORD_REGEX)
    val m = p.matcher(this)
    return m.matches()
}

/**
 * ip地址正则检查
 */
fun String.checkIp(): Boolean {
    val p = Pattern.compile(IP_REGEX)
    val m = p.matcher(this)
    return m.matches()
}