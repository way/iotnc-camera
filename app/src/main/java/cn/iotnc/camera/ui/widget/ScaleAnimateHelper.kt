package cn.iotnc.camera.ui.widget

import android.animation.ValueAnimator
import android.view.View

class ScaleAnimateHelper(var target: View, override var state: Int) : AnimateHelper {

    override fun show() {
        val va = ValueAnimator.ofFloat(target.scaleX, 1f).apply {
            duration = 300
        }
        va.addUpdateListener {
            val scale = it.animatedValue as Float
            target.scaleX = scale
            target.scaleY = scale
        }
        va.start()
        state = STATE_SHOW
    }

    override fun hide() {
        val va = ValueAnimator.ofFloat(target.scaleX, 0f).apply {
            duration = 300
        }
        va.addUpdateListener {
            val scale = it.animatedValue as Float
            target.scaleX = scale
            target.scaleY = scale
        }
        va.start()
        state = STATE_HIDE
    }

}