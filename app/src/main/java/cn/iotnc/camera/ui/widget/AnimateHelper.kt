package cn.iotnc.camera.ui.widget

const val STATE_SHOW = 1
const val STATE_HIDE = 0

interface AnimateHelper {
    var state: Int
    fun show()

    fun hide()
}