package cn.iotnc.camera.ui.display

interface IVLCPlayer {
    fun setListener(onLoading: () -> Unit, onSuccess: () -> Unit, onError: () -> Unit)

    fun playVideo(url: String)

    fun play()

    fun pause()

    fun stop()

    fun isPlaying(): Boolean

    fun release()
}