package cn.iotnc.camera.ui.main

import androidx.databinding.DataBindingUtil
import cn.iotnc.camera.R
import cn.iotnc.camera.databinding.ItemCameraManagerBinding
import cn.iotnc.camera.model.CameraBean
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class CameraManagerAdapter :
    BaseQuickAdapter<CameraBean, BaseViewHolder>(R.layout.item_camera_manager, null) {

    override fun onItemViewHolderCreated(viewHolder: BaseViewHolder, viewType: Int) {
        DataBindingUtil.bind<ItemCameraManagerBinding>(viewHolder.itemView)
    }

    override fun convert(holder: BaseViewHolder, item: CameraBean) {
        holder.itemView.tag = item
        val binding = DataBindingUtil.getBinding<ItemCameraManagerBinding>(holder.itemView)
        binding?.let {
            it.record = item
            it.executePendingBindings()
        }
    }
}