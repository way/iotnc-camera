package cn.iotnc.camera.model

import android.os.Parcelable
import androidx.annotation.IntDef
import androidx.annotation.Keep
import androidx.annotation.StringDef
import com.google.gson.JsonParseException
import kotlinx.android.parcel.Parcelize
import org.json.JSONException
import retrofit2.HttpException
import java.io.IOException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.text.ParseException

class IotncThrowable(
    val code: Int,
    override val message: String? = null,
    override val cause: Throwable? = null
) : Throwable(message, cause) {

    companion object {
        /**
         * 响应成功
         */
        const val SUCCESS = 0

        /**
         * Token 过期
         */
        const val TOKEN_INVALID = 1006

        /**
         * Token 为空
         */
        const val TOKEN_EMPTY = 1007

        /**
         * 未知错误
         */
        const val UNKNOWN_ERROR = 1002

        /**
         * 服务器内部错误
         */
        const val SERVER_ERROR = 1003

        /**
         * 网络连接超时
         */
        const val NETWORK_ERROR = 1004

        /**
         * API解析异常（或者第三方数据结构更改）等其他异常
         */
        const val API_ERROR = 1005

        fun Throwable.handleException(): IotncThrowable {
            this.printStackTrace()
            return when (this) {
                is IotncThrowable -> this
                is SocketTimeoutException, is IOException, is ConnectException, is HttpException, is UnknownHostException -> IotncThrowable(
                    NETWORK_ERROR,
                    "网络连接异常",
                    this
                )
                is JsonParseException, is JSONException, is ParseException -> IotncThrowable(
                    SERVER_ERROR,
                    "数据解析异常",
                    this
                )
                is IllegalArgumentException -> IotncThrowable(SERVER_ERROR, "参数错误", this)
                else -> IotncThrowable(UNKNOWN_ERROR, "抛锚了~", this)
            }
        }
    }
}

@Keep
data class BaseResponse<T>(
    val code: Int,
    val errorCode: Int,
    val errorDesc: String?,
    val `data`: T?
)

@Parcelize
@Keep
data class CameraBean(
//    val after: Int, // 25
//    val bg: Int, // 0
//    val bitrate: Int, // 6488
//    val brightness: Int, // 100
//    val delay: Int, // 0
//    val denoise: Int, // 50
//    val expMode: Int, // 1
//    val framerate: Int, // 25
//    val gain: Int, // 30
//    val gateway: String, // 192.168.9.1
//    val h0: Int, // 69074
//    val h1: Int, // 3743
//    val h2: Int, // -13789200
//    val h3: Int, // -2327
//    val h4: Int, // 67733
//    val h5: Int, // 2543310
//    val h6: Int, // 0
//    val h7: Int, // 0
//    val h8: Int, // 65538
//    @SerializedName("hf_switch")
//    val hfSwitch: String, // OFF
//    val identity: String, // slave
    val ip: String, // 192.168.9.119
    //val mac: String, // 0a:1a:2a:3a:4f:fc
//    val netmask: String, // 255.255.0.0
//    val nrMode: Int, // 1
//    val pmfSwitch: Boolean, // false
//    val resolution: Int, // 1
//    val rg: Int, // 0
    val rtspUrl: String, // rtsp://192.168.9.119/ch1
//    val sdFreeSpace: String, // 62493040640
//    val sdTotalSpace: String, // 62724411392
//    val shutter: Int, // 7
    val version: String, // 1.9.8
//    val wbMode: Int, // 0
    @WorkMode val workMode: String // jpg
) : Parcelable {
    //工作类型
    @StringDef(
        WorkMode.JPG,
        WorkMode.RTSP
    )
    @Retention(AnnotationRetention.SOURCE)
    annotation class WorkMode {
        companion object {
            const val JPG = "jpg"
            const val RTSP = "video"
        }
    }
}