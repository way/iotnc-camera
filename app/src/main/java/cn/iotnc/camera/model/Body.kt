package cn.iotnc.camera.model

import androidx.annotation.Keep

@Keep
data class SearchBody(val magic: String = "20200101")

@Keep
data class PictureBody(val index: Int = 0x7fffffff)